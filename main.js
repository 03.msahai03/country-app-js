const countries = document.querySelector(".list");
const details = document.querySelector(".detail");
const borderingCountries = document.querySelector(".bordering");
const searchCountry = document.querySelector("input[type = 'search']");

const urlSearchParams = new URLSearchParams(window.location.search);
const code = urlSearchParams.get("code");
console.log(code);

const getCountries = (event) => {
  const API_URL = `https://restcountries.eu/rest/v2/all`;

  fetch(API_URL)
    .then((res) => res.json())
    .then((data) => {
      data.forEach((element) => {
        let country = document.createElement("div");
        let img = document.createElement("img");
        // let countryInfo = document.createElement("div");

        country.classList.add("card", "country", "p-2", "mx-auto");
        // countryInfo.classList.add("container", "p-5");
        // img.src = element.flag;

        country.appendChild(img);
        // country.appendChild(countryInfo);
        countries.appendChild(country);

        country.addEventListener("click", function (e) {
          window.location.href = `detail.html?code=${element.alpha3Code}`;
        });

        country.innerHTML = `
        <img src = ${element.flag} />
        <h3 class = "p-2 text-center">${element.name}</h3>
        <p>Currency: ${element.currencies[0].name}  </p>
				<p>Region: <span>${element.region}, ${element.subregion}</span></p>
        `;
      });
    });
};
if (!code) {
  getCountries();
}

const getCountry = (event) => {
  const API_URL = `https://restcountries.eu/rest/v2/alpha/${code}`;

  fetch(API_URL)
    .then((res) => res.json())
    .then((data) => {
      // console.log(data);
      let country = document.createElement("div");
      let img = document.createElement("img");

      details.classList.add("card", "border");
      country.appendChild(img);

      // console.log(data.borders.length);

      let borderArr = [];
      data.borders.map((country) => {
        // console.log(country);
        borderArr.push(country);
      });

      // let flagArr = [];
      // data.borders.map((country, index) => {
      //   // console.log(country);
      //   fetch(`https://restcountries.eu/rest/v2/alpha/${country}`)
      //     .then((res) => res.json())
      //     .then((data) => {
      //       // console.log(index);
      //       borderArr[index] = data.name;
      //       flagArr[index] = data.flag;
      //     });
      // });

      console.log(borderArr);

      details.appendChild(country);
      country.innerHTML = `
      <h2 class = "px-5 py-2 text-center display-4 my-3">${data.name}</h2>

      <div class="container d-flex">
      <div class="row">
      <div class="col-md-6 mb-5">
      <img src = ${data.flag} class="detailFlag" />
     </div>
     <div class="col-md-6 mb-5">
     <div class="container py-3 px-5">
     <h6><span class="fw-bold">Native name: </span> ${
       data.nativeName
     }</h6>      
     <h6><span class="fw-bold">Capital: </span> ${data.capital}</h6>      
     <h6><span class="fw-bold">Population: </span> ${data.population}</h6>      
     <h6><span class="fw-bold">Region: </span> ${data.region}</h6>      
     <h6><span class="fw-bold">Sub-region: </span> ${data.subregion}</h6>      
     <h6><span class="fw-bold">Area: </span> ${data.area}</h6>      
     <h6><span class="fw-bold">Country Code: </span> ${data.callingCodes}</h6>
     <h6><span class="fw-bold">Languages: </span> <span>${data.languages.map(
       (language) => language.name
     )}</span></h6> 
     <h6><span class="fw-bold">Currencies: </span> <span>${data.currencies.map(
       (currency) => currency.name
     )}</span> </h6>
     <h6><span class="fw-bold">Top Level Domain: </span> ${
       data.topLevelDomain
     }</h6>
    </div>
    </div>

   <h3 class="px-5 py-2 mt-5">Border Countries:</h3>
   <div class="container d-flex">
     <div class="row">
       <div class="bordering">
       ${borderArr
         .map(
           (border) => `
           <img src = "https://restcountries.eu/data/${border.toLowerCase()}.svg" class="borderImg m-3 border"/>  
            `
         )
         .join("")}
       </div>
     </div>
   </div>
 </div>
      </div>
        
      `;

      const borderCountries = document.querySelector(".bordering");
      console.log(borderCountries);

      borderCountries.addEventListener("click", (event) => {
        const api_u = `https://restcountries.eu/rest/v2/alpha/${event.target.innerHTML.trim()}`;
        console.log(event.target.innerHTML);
        // console.log(api_u);

        fetch(api_u)
          .then((res) => res.json())
          .then((data) => {
            console.log(data);
            // window.location.href = `detail.html?code=${data.alpha3Code}`;
          });
      });
    });
};

searchCountry.addEventListener("input", (event) => {
  const searchVal = event.target.value.toLowerCase().trim();

  const filteredCountries = Array.from(document.querySelectorAll("h3"));

  filteredCountries.forEach((country) => {
    const mySearch = country.innerHTML.toLowerCase().trim();
    if (mySearch.includes(searchVal)) {
      country.parentElement.style.display = "flex";
    } else {
      country.parentElement.style.display = "none";
    }
  });
});

const getFlag = (code) => {
  const API_URL = `https://restcountries.eu/rest/v2/alpha/${code}`;
  fetch(API_URL)
    .then((res) => res.json())
    .then((data) => {
      console.log(data.flag);
      return data.flag;
    });
};

// const getBorders = (event) => {
//   const API_URL = `https://restcountries.eu/rest/v2/alpha/${code}`;

//   fetch(API_URL)
//     .then((res) => res.json())
//     .then((data) => {
//       let borderArr = [];
//       let flagArr = [];
//       data.borders.map((country) => {
//         // console.log(country);
//         fetch(`https://restcountries.eu/rest/v2/alpha/${country}`)
//           .then((res) => res.json())
//           .then((data) => {
//             console.log(data);
//             borderArr.push(data.name);
//             flagArr.push(data.flag);
//           });
//       });

//       let border = document.createElement("div");

//     });
// };

if (code) {
  getCountry();
  // getBorders();
}
